#  redmine_mit_auth: provides SSL/Touchstone authentication for redmine
#  Copyright (C) 2012 Josh Bialkowski (jbialk@mit.edu)
#  Modified Copyright (C) 2013 Aaron Herting (adh2380@rit.edu)
# 
#  This program is free software: you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
# 
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
# 
#  You should have received a copy of the GNU General Public License
#  along with this program.  If not, see <http://www.gnu.org/licenses/>.
# 
require 'redmine'

Redmine::Plugin.register :redmine_mit_auth do
  name        'Redmine MIT auth plugin (Modified for RIT)'
  author      'Josh Bialkowski (RIT Mod: Aaron Herting)'
  description 'Enable authentication using Touchstone (Shibboleth) SSO or MIT certificates (Modified for RIT)'
  version     '0.0.2'
end
